#include <iostream>
#include <math.h>

using namespace std;

void primo(int* numero) {

    int* dos = new int(2);
    int* raiz = new int(sqrt(*numero));

    if (*numero == 1)
    {
        cout << "\nNo es PRIMO" << endl;
    }
    else
    {
        while (*numero % *dos != 0 && *numero % *raiz != 0)
        {
            ++* dos;
        }

        if (*dos == *numero)
            cout << "\nPrimo" << endl;
        else
            cout << "\nNo es primo" << endl;
    }

    delete dos;
    delete raiz;
}

int main() {

    int* numero = new int;

    cout << "Escriba el numero: ";
    cin >> *numero;

    //Numero primo
    primo(numero);

    delete numero;

    system("pause");
    return 707;
}
